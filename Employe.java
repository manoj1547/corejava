package com.java.practice;  // created  package
 
public class ConstructorPractice{  // creating a class name as constructorpractice

//private access specifiers can't be accessed outside the class if we want to access it means we should use getters and setters method
	 protected  int employeId;  //instance variables declared as protected--> can be accessed outsid the package also through inheritance
	 protected  int employeno;  //instance variable declared as protected --> can be accessed outsid the package also through inheritance
	 private String employename; //instance variable declared as private  -->can't be accessed outsid the package through inheritance
	  protected double employesalary;  //instance variable declared as protected --> can be accessed outsid the package also through inheritance


}
=======================================================================================================
=======================================================================================================


import com.java.practice.ConstructorPractice;  //importing above package inside this package by importing



public class Employe extends ConstructorPractice {  // here employe class inheriting the properties of constructorpractice class

public static void main(String[] args) {  // main fuction where jvm will execute the things

		Employe e = new Employe();  // creating the object of child class
			e.employeId= 123;     // initializing the variables by using object reference
			e.employeno= 223344;   //initializing the variables by using object reference
			e.employesalary=5000.00;  //initializing the variables by using object reference
			
			System.out.println(e.employeId);  // invoking the function and printing
			System.out.println(e.employeno);  //invoking the function and printing
			System.out.println(e.employesalary);  //invoking the function and printing
			
			
			
			

		}

	}

