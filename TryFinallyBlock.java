public class TryFinallyBlock {  //created class identifier

	public static void main(String[] args) {  // main method where execution starts
		int a [] = {1,2,3,4,5,6,7};   // initializing and storing in array 
		
		
		try  // try block to handle the exception
		{
			if(a.length!=7)  //giving if condition to check the length is not equal to 7
				throw new ArraySizeException("The size of the array should be 7");  //throw keywords are used to throw the exception statement

		}
		catch(Exception e)  //this block will get executed only when try block gets the exception
		{
			System.out.println(e);  //printing statement on console
		}
		finally   // irrespective of exception finally block will get always executed
		{
			System.out.println("inside finally block");  //printing statement on console
			
		}
		System.out.println("after all blocks");  //printing statement on console

	}


}
