//String Program to add each digit in the given string

public class StringEachNUmber {   // class identifier

	public static void main(String[] args) {  //main function where jvm will understand and execute the things
		String s = "abcd12cbg45ghj34hj";   // string intialized some value
	    int	sum = 0;        //created int variable and initialized as 0
	    String sp="";       // one more string variable initialized as empty 
	    boolean flag = false;   //boolean type variable initilized as false
	for (int i=0; i<s.length(); i++)    // iteration
	{
        if(Character.isDigit(s.charAt(i))) //used Character wrapper class and is digit function
        	                               //it will get the digits from the given string
        {                                  //passing charAt(i) as argument which will consider numeric character only in the given string
        	
        	sp = sp +s.charAt(i);   //sp= ""+12 + "" +45+ "" +34
        	flag = true;     // it will true until if condition get numeric char once it goes to other char it gives false it exists from the if condition
        	                 
        }
        else if (flag)      // once if condition is false and exit from that then will enter inside the else if condition
        {
        	sp = sp+" ";    // 12+""+ 45+"" +34+""
        	flag = false;   //after one  space it will exit from this since we have intialized as flag as false each time it enters if else condtion
        }
	}
        
        System.out.println(sp);   //printing statement to print
        
        String sr[] = sp.split("\\s");    // it will handle the spaces and store inside the array
        
        for(int i = 0; i<sr.length; i++)   //iteration by considering array length function
        {
        	sum = sum+ Integer.parseInt(sr[i]);  // sum = 0+12+45+34
        	                                     // parseInt --> will convert the string type to integer type
        }
        
        System.out.println(sum);    //printing statement to print
	}
}
        	
     