public class StringAlphaNumeric {  // created class identifier

	public static void main(String[] args) {  // main function where execution starts
		String s = "abcd12cbg45ghj34";    // string assigned some value
		
		int sum =0; // initilizing sum as 0
		
		for(int i=0; i<s.length(); i++)  // iteration
		{
			if(Character.isDigit(s.charAt(i)))  // branching statement to check the numeric character in the given string
				                                //Character is a wrapper class and is digit is a function to check the character is digits or not 
				                                //  charAT(i) function will get the numeric character in given string
			{
				sum = sum + Character.getNumericValue(s.charAt(i));  //  getnumericvalue function will returns the int value
				                                                     //s.charat function it will returns the character after checking in specified index 
			}
		}
		System.out.println(sum);  // printing statement on console

	}

}
