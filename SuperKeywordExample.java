class Superrk  //class identifier or parent class
{
	public Superrk()  // parent class constructor
	{
		System.out.println("inside the parent class");  //print statement for printing in console
	}
}
public class SuperKeywordExample extends  Superrk {  // child class inheriting the properties of parent class
	
	public SuperKeywordExample()  // child class constructor
	{
		super();  // super keyword will invoke the immediate parent class contructor inside the child class
		System.out.println("inside the child class");  //print statement for printing in console
	}

	public static void main(String[] args) {  //main function
		SuperKeywordExample s = new SuperKeywordExample(); //creating an object reference for child class and invoking the child class constructor

	}

}