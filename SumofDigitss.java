// find the sum of given digits

public class SumofDigitss
{

public static int getSumofDigits(int input)
{

 int sum =0;
while(input> 0) // to perform the iteration
{
  sum = sum + input % 10; // this logic is used to get the last digit and to shift it to fist digits place
  input = input /10;  // this logic is used to remove the last digit
}

if(sum>9) // to perform the recursion 
{
  System.out.println(sum);
   input = sum; // swapping the input value to the sum since latest value of sum is zero
   return getSumofDigits(input);
}
else
{
  return sum;
}
}

public static void main(String [] args)
{
  int  n =12345; // initializing the value to the variable n
 int result = getSumofDigits(n); // passing the value to the calling method and storing it in variable result
System.out.println(" the sum of individual digits is: " + result);
}

}
