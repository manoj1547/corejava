public class Box { // creating class 
	
	int width; // instance variables 
	int height;
	int depth;
	

	public Box(int width, int height, int depth) { // parameterized constructor 
		super();
		this.width = width;      //this keyword will hepls us to assign the values to the arguments passing 
		this.height = height;   // it will differenciate the instance varaibles and arguments in constructor to assign the values
		this.depth = depth;
	}

	public int volume()   // method with int return type
	{
		int vl = width*height*depth;   // logic to get the volume of the box 
		return vl;    // it will return the value after applying the above logic
		
		
	}

	public static void main(String[] args) {    // main method where jvm will start execute the things
		Box b = new Box(5,5,10);   // crating the object for class box and assigning the values to the arguments
		b.volume();  // invoking the function
		System.out.println("the dimension of the given box is :" +  b.volume() );  // it will print the statement inside it contains
	}
		
		
		
	
		

	}