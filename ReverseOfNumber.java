//How to print the reverse of the given number

public class ReverseOfNumber
{

public static void main(String [] args)
{

int n = 24198;

int rev =0;

while(n>0)
{
  rev = rev * 10+ n % 10;    // this logic is used to get the last digit and shift it to first digits place.
   n = n/10;    // this logic is used to remove individual digit from the given input after applying the above logic.
}

  System.out.println("The reverse of the given number: " +rev);
}

}