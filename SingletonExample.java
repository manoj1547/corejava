class SingletonEXP  // creating a class identifier
{
	
	private SingletonEXP()   // created a constructor with private keyword so outside the class we cant acess it
	{
		
	}
	
	private static SingletonEXP s = null; // created an object reference of class and initilized it as null
	
	public static SingletonEXP getInstance()  // static method with return type and used getinstance method
	{
		if(s==null)  // giving condition to check whether s is equal to null or not
		
			s = new SingletonEXP();  // if s is equal to null then object will be created for s object reference
	
		
		return s;  //it will return the value of s 
	}
	

}

 public class SingletonExample {  // creating a class identifier
		
		public static void main(String [] args)  // main method where execution starts
		{
			SingletonEXP s1 =  SingletonEXP.getInstance();  // invoking the class followed by method name becz it is in different class
			SingletonEXP s2 =  SingletonEXP.getInstance();  // invoking the class followed by method name
			System.out.println(s1);  //print statement on console
			System.out.println(s2);  // print statement on console
		}
 }
		