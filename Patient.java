//logical thinking problems, problem number 3.

public class Patient { // creating a classname  as patient
	
	String patientName; // instance variables
	double height;      // instance variables
	double weight;      // instance variables
	
	public double computeBMI()  // creating a method called comuteBMI
	{
		
		double BMI = weight / (height*height);   // logic to get the result of BMI 
				return BMI;                // this will return the BMI after applying the above logic
		
	}

	public static void main(String[] args) {   // main method where jvm will understand the things and execute it
		Patient p = new Patient();         // creating an object for patient class
		p.patientName= "swapna";         // by using object refernce initializing the name 
		p.height = 4.5 ;               //by using object refernce initializing the value to the height
		p.weight = 60;               // by using object refernce initializing the value to the weight
		p.computeBMI();            // by using object refernce invoking the method computeBMI
		
		System.out.println(p.patientName +" health codition of BMI is " +p.computeBMI());  // this will print the statement which is inside

	}

}