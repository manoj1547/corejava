// inheritance program

class A{   // class identifier 
int m;    // instance variable
int n;

void display()  // method without return type
{
System.out.println("m and n are :"+m+" "+n);  // this will print the statement inside it contains
}
}
class B extends A {    // class B inheriting the properties of class A through extends keyword
int c;               // instance variable

void display1()      //method without return type
{
System.out.println("c:" + c);  //this will print the statement inside it contains
}

void sum()   //one more method created inside the class B
{
System.out.println("m+n+c =" +(m+n+c)); //this will print the statement inside it contains
}
}

class InheritanceDemo {     // class with name
 
public static void main(String [] args)   // main method which jvm will consider the things to execute
{
     A a = new A();   // object creation for class A
     B b = new B();   // object creation for class B
      
a.m = 10; b.n = 20;    //initializing the values to the variables of class A
System.out.println("state of object A:");   // printing statement
a.display();                        // invoking the function by using object reference
b.m = 7; b.n = 8; b.c = 9;         //initializing the values to the variables of class B
System.out.println("state of object B:"); //printing statement
b.display();  // invoking the function of class A by using object reference of class B
b.display1();  //invoking the function of class B by using object reference of class B
System.out.println("sum of m,n and c in object Bis:");  //printing statement
b.sum(); // invoking the funcyion through object reference of class B
}
}