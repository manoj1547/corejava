// logical thinking problems, problem number 2

public class Calculator {  //creating  a class called calculator
	
	public static double powerInt(int num1, int num2)  //creating a method with return type double and arguments
	{
		double m = Math.pow(num1, num2); // using inbuilt method of math.pow to calculate the power of num
		return m;         // it will return the value after power calculation
	}
	
	public static double powerDouble(double num1, int num2)  //creating a method with return type double and arguments
	{
		double m2 = Math.pow(num1, num2);   // using inbuilt method of math.pow to calculate the power of num
		return m2;       //it will return the value after power calculation
	}

	public static void main(String[] args) {  //main method where jvm will understand and execute the things
		 powerInt(4,4);    //invoking the method by passing suitable arguments
		powerDouble(5.5,10);   //invoking the method by passing suitable arguments
		
		System.out.println(powerInt(4,4)+" "+ powerDouble(5.5,10) ); //printing the statements inside
	
		

	}

}
