// program to find first non repeatative character in the given string

public class NonRepeatedCharacter {   // class identifier

	public static void main(String[] args) {   // main function where execution starts
	    String s = "amazoma";   // string variable with initilization
	    
	    for(int i=0; i<s.length(); i++)     //iteration --> s.length will give total character present in the given string
	    {
	    	if(s.indexOf(s.charAt(i))==s.lastIndexOf(s.charAt(i)))  // here we are comparing first character and last character is same or not by using index function 
	    			{
	    		       System.out.println(s.charAt(i));  //those character same that character will be printed on console
	    		      break;    // since we are finding only first nonrepeatative char. we are using break statement--> after getting first non repeative char it will break and exit from the loop
	    		       
	    			}
	    }

	}

}
