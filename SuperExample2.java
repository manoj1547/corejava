class Test  // parent class
{
	public Test(int id, int age, float money)  // parent class constructor with arguments
	{
		System.out.println("i am parent constructor"); //statement to print on console
	}
}
public class SuperExample2 extends Test {  // child class
	
	public SuperExample2(int id, int age)  // child class constructor with different arguments
	{
		super(1234, 29, 100.05f); //super statement is invoking the parent class constructor with respective argument passing 
		System.out.println("i am child comstructor"); // statement to print on console
	}

	public static void main(String[] args) {  // main function where execution happens
		SuperExample2 se = new SuperExample2(1234, 30); // creating an object to invoke the child class constructor

	}

}
