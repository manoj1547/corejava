public class PalindromeString {  // creating a class identifier

	public static void main(String[] args) {  // main function where execution starts
		String a = "amma";    // string variable intialized as amma
		StringBuffer rev = new StringBuffer(a);  // since string doesnot have inbuilt function to reverse
		                                         // we are converting string to stringbuffer so we can make use of inbuilt function to reverse the string
		String r = rev.reverse().toString();   // inbuilt method to reverse the given string 
		                                       // to string will convert the stringbuffer to string in output
         
         
         if(r.equals(a))  // we are using condition to check whether reverse string is equals to actual given string 
         {
        	 System.out.println("the given string + a + is palindrome");  // if the above condition is satisfied this statement will get executed
         }
         else  // or condition
         {
        	 System.out.println("the given string + a + is not a palindrome");  // the above if condition is not satisfied then this statement eill get executed
         }
	}

}