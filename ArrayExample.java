// array is used to store the group of similar datatypes
// size is fixed , we cant change after we initialized the size

public class ArrayExample {   //creating a class identifier

	public static void main(String[] args) {  // main function where jvm will execute the output by understanding the code
		
		int a[] = new int [200];  // array syntax to store the values upto 200
		
		for (int i=0; i<a.length; i++)  //iteration to store the values by applying some logic in array
		{
			a[i] = (i+1)*10;    // a[0]=0+1*10=10
		}                       // a[1]= 1+1*10=20
		                        // a[2]=2+1*10=30
		                        // a[3]=3+1*10 =40
                                // will continuew until the condition is satisfied (a[199]=199+1*10 =2000)
		
		for (int j=0; j<a.length; j++)  //to retrieve the stored array data
		{
			System.out.println(a[j]);   // it will print the statement which it contains inside
		}
	}

}