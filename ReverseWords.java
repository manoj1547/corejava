// reversing words by using string and array

public class ReverseWords {  // created class identifier

	public static void main(String[] args) {  //main function where execution starts
		String s = "learning is my passion";  // string value
		String a[] = s.split("\\s");  // it will split the words by spaces in the given string
                                        //it handles the spaces
		String rev="";
		for(int i=a.length-1; i>=0; i--)  //iteration--> until condition is satisfied it will be keep on running
			                              
		{
			rev = rev + a[i] + " ";  //applying logic to get reverse of string
		}
		System.out.println(rev); // it will print the statement inside it contains
	}

}
