 class Encapsulation { // class name
	

	public Encapsulation() { // default constructor or zero argument constructor in which we can;t pass arguments
                                // it will be invoked once the object is created
		
	}
	
	private int id; // instance variables
	private int age;
	private String name;

	public int getId() { // getters method or function or member function 
		return id;     // it will return the id once it get invoked
                           // getters method is used to retrieve or to get the data
	}

	public int getAge() { // to get the age 
		return age;     // it will return the age once it get invoked
	}

	public String getName() { // to get the name
		return name;         // it will return the name once it get invoked
	}

	public void setId(int id) {  // setter function to set the data
		this.id = id;         // to set the id
	}

	public void setAge(int age) { // to set the age
		this.age = age;
	}

	public void setName(String name) { // to set the name
		this.name = name;
	}
}
	
	 class EncapsulationMain{   another class of different class name

	public static void main(String[] args) {   // main method in which execution starts
		Encapsulation e = new Encapsulation();  // creating an object for encapsulation class
		e.setId(777);  // by using the object reference setting the id
		e.setAge(29);  // by using the object reference setting the age
		e.setName("swapna");  // by using the object reference setting the name
		
		System.out.println("name :"+ e.getName());  // printing statement - this will print the name
		System.out.println("id :"+ e.getId());      // this will print the id
		System.out.println("age :"+ e.getAge());    // this will print the age
		
	}


	}


