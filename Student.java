// program for method overriding 

 class Teacher {  // creating a class
	
	public void school()   // method or function without return type
	{
		System.out.println("knowledge is power");  // printing statement, it will print the statement inside
	}
   
	public void play()  // fuction of same class without return type
	{
		System.out.println("play good"); //printing statement, it will print the statement inside
	}
}

	
	 class Student extends Teacher {  //class student inheriting the class teacher by using extends keyword
		
		@Override
		public void school()   // method overriding
		{
			System.out.println("aquiring right knowledge is powerful"); //printing statement, it will print the statement inside
		}
	
	
	public static void main(String[] args) {  // main funcion where jvm will understand and execute the things
		Teacher Tr = new Student();  //here by using parent object reference pointing to child class
		Tr.school();   // by using the parent object reference we can invoke the overriden methods only

	}

	}