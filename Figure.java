abstract class Area{   // creating an abstract class
int dimensionX;  // instance variables
int dimensionY;

	abstract double area();  // creating an abstract method with only declarartion and no implementation
		
	}
class Rectangle extends Area{  // another class inheriting the properties of abstract class
	 double area(){   // method overriding --> ovveriding the abstract method and providing the implementation
		 
		System.out.println("the area of rectangle is : ");   // printing statement
		return dimensionX * dimensionY;    //return statement , it will return the result once the method got invoked
		 
	 }
}

class Square extends Area{  // another class inheriting the properties of abstract class
	double area() {   //method overriding --> ovveriding the abstract method and providing the implementation
		System.out.println("the area of square is : ");  // printing statement
		return dimensionX * dimensionY;   //return statement , it will return the result once the method got invoked
	}
}



public class Figure {  //creating one more class

	public static void main(String[] args) {  // main method where jvm will understand and execute the things
		
		Rectangle r = new Rectangle();   // creating an object for class rectangle
		r.dimensionX=4;     // initializing the values through object reference
		r.dimensionY = 8;   // initializing the values through object reference
		System.out.println(r.area());  //printing statement
		
		
		 Square s = new  Square();  //creating an object for class rectangle
		 s.dimensionX=8;   //initializing the values through object reference
		 s.dimensionY=8;   //initializing the values through object reference
		 System.out.println(s.area());  // printing statement
		
		
		
	}
	
	

}
