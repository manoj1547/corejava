import java.io.BufferedReader; 
import java.io.IOException;
import java.io.InputStreamReader;

public class ReadlineMethodExp { //created a class identifier

	public static void main(String[] args) throws IOException {  // main method where execution starts
		 String s = "";  //string wrapper class initialized as empty
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));  //bufferreader is faster than scanner class
		                                                                           //creating object reference to bufferreader class
		                                                                           //and passing inputstreamreader to read the input
		s=bf.readLine(); //readline method will read string only from input
		
		StringBuffer sb = new StringBuffer(s);  //created object reference for stringbuffer and passing s
		
		System.out.println(sb.reverse());  //it will print statement on console by reversing the given string

	}

	
}
