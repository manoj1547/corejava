import java.util.Scanner;  //importing scanner class from java.util package

public class ReadingFromKeyboard {  // created a class identifier 

	public static void main(String[] args) {  // main method where execution starts
		String s = "";  // string wrapper class initialized as empty
	Scanner	s1 = new Scanner(System.in);  // creating an object reference for scanner class 
	                                      //and passing system.in since we reading from keyboard
	s= s1.next(); // assigning s1.next method to s
	
	StringBuffer sb = new StringBuffer(s); // creating object reference to stringbuffer class and passing s
	System.out.println(sb.reverse());  // statement to print on console
				

	}

}