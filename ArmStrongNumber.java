//ArmStrong Number--> input and output should be same

public class ArmStrongNumber
{

public static int calculatepower(int a, int l) //a is the variable for which we need to determine the power and l is the how many time power calculated
{
  int c = 1;
for(int i=0; i<l; i++) 
{

   c = c*a; // a = 3 , c= 1*3=3*3=9*3=27*3....
}
   return c;
}

public static void main (String [] args)
{

   int n = 153;
   int temp = n;
   int length = 0;
   int output = 0;

while(temp>0)
{
   temp = temp/10; // logic is used to find the length
    length++;
}

 temp = n; // assigning the variable n to temp again since updated temp value is zero

while(temp>0) //iteration
{
  output = output + calculatepower(temp%10, length); //logic is used to find the power of individual digits by length
   temp = temp/10; // to remove the last digit
}

if(n==output) //comparing output is equal to input or not
System.out.println("the given number is armstrong number");
else
System.out.println("the given number is not an armstrong number");
}
}
   