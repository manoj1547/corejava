//program for this keyword to call another constructor and to differentiate the instance variable from arguments

public class THisKeywordExample {   // creating a class
	
	private String name;   // instance variables declared as private
	private int salary;    // we can access only within the class of same method
	private int id;
	
	

	public THisKeywordExample() {   //default constructor which can be invoked when object is created
		System.out.println("inside default constructor");  // this will print the statement inside
		
	}
	
	public THisKeywordExample(String name, int salary, int id) {  // all argument construcor which has the arguments same as instance variable 
		 this();           // calling the default constructor and to call that this should be the first statement inside the methodbody
		this.name = name; 
		this.salary = salary;    // this keyword will help to differnciate the instance varaibles and arguments since both has the same name
		this.id = id;
		System.out.println("inside all argument constructor");  // this will print the statement inside 
	}

	public static void main(String[] args) {  //main function where execution happens
		
		THisKeywordExample Th = new THisKeywordExample("swapna", 50000, 12345);   creating an object for invoking the argument constructor

	}

}
