import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.Reader;

public class CompileTimeException {   //created a class identifier

	private static Reader FileReader;  

	public static void main(String[] args) { //main method where execution starts
		
		 File file = new File("C:\\Users\\SWAPNAMANU\\Desktop\\JAVA CODE\\ImmutableClass ");  //file class imported from java.io
		                                                                                      //passing path of the file
		 try   //try block to handle the exception
		 {
		 FileReader fb = new FileReader(file);   //using Filereader class from java .io.filereader and passing the file to read
		  
		 }
		 catch(Exception e)  // catch block will execute the remaining set of statements without terminating after exception occurs
		 {
			 System.out.println(e);   //it will print the given statement on console
		 }
		 
		 finally  // finally block will get executed irrespective of exception occurs or not
		 {
			 try//one more try block inside finally block to handle the exception of closing filereader
			 {
			 FileReader.close(); //close method used to close the filereader
			 }
			 catch(Exception e) //catch block will execute the remaining set of statements without terminating after exception occurs
			 {
				 System.out.println(e);    //it will print the given statement on console
			 }
		 }
		 

	}

}
