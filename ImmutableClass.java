//program to learn how to create our own immutable class

public final class ImmutableClass {   //creating a class as final so we can not inherit the class
	
	private final int id;  //instance variables declared as final--> so once it is initialized we cant change the value
	private final String name;  //instance variables declared as final--> so once it is initialized we cant change the value
	private final int salary;   //instance variables declared as final--> so once it is initialized we cant change the value
	
	

	public ImmutableClass(int id, String name, int salary) {  //calling all argument constructor to intialize the value
		super();  
		this.id = id;  //this keyword will differentaite the argument with variable to intialize the value
		this.name = name;  //this keyword will differentaite the argument with variable to intialize the value
		this.salary = salary;  //this keyword will differentaite the argument with variable to intialize the value
	}



	public int getId() {  //to retrieve the value of id
		return id;
	}



	public String getName() {  //to retreive the value of name
		return name;
	}



	public int getSalary() {  //to retrieve the value of salary
		return salary;
	}



	public static void main(String[] args) {  //main function where jvm will execute the things by understanding logic
		
		ImmutableClass i = new ImmutableClass(123, "swapna", 50000);  //creating an object reference for immutableclass
	System.out.println(i.getId());  //by using object reference invoking the getter funtion and printing it
	System.out.println(i.getName());  //by using object reference invoking the getter funtion and printing it
	System.out.println(i.getSalary()); //by using object reference invoking the getter funtion and printing it
	
	
		
	
		

	}

}
