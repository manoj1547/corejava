interface add  // creating an interface
{
	public void count(); //method declarartion without implementation and in interface by default implicitly compiler will
                           // consider it as public static abstract method only
}
interface sum // another interface
{
	public void count(); //method declaration without implementation and has the same name as interface add method
}
public class CalculateSum implements add,sum{ // calculatesum class implements the add and sum interface 
                                                // it is possible to implements multiple inheritance in java
	@Override
	public void count() // method overriding means providing the implementation for the abstract methods
	{
		System.out.println("The count of two numbers:" + (50+100)); // print statement
	}
	public static void main(String [] args) // main funcion
	{
		add a = new CalculateSum(); //by using the interface reference pointing the class 
		a.count();                   // invoking the overriden method of interface 
		sum s = new CalculateSum();  //by using the interface reference pointing the class 
		s.count();                   //invoking the overriden method of interface
	}

}