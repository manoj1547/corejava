// reversing string by not using inbuilt function


public class ReverseString2 {  // created a class identifier

	public static void main(String[] args) {  //main function where jvm will execute the output by understanding
		
		String s = "swapna";  // string varaible
		//index number s w a p n a --> length of character is 6
		           //  0 1 2 3 4 5
		
		String reverse = "";  //one more variable reverse initialized it as empty to get the reverse of string
		
		//StringBuffer sb = new StringBuffer(s);
		//sb.reverse().toString();
		
		for (int i= s.length()-1; i>=0; i--)  //iteration i.e., 1) i = 6-1 = 5; 5>=0 = true; 5-- will continue until the condition get satisfied
			                                                  
		{                                       
			                                   // iteration to get the reverse string
			reverse = reverse + s.charAt(i);   // 1) reverse + s.charAt(5) = a
			                                   // 2) reverse + s.charAt(4) = an
			                                   // 3) reverse + s.charAt(3) = anp
			                                   // 4) reverse + s.charAt(2) = anpa
			                                   // 5) reverse + s.charAt(1) = anpaw
			                                   // 6) reverse + s.charAt(0) = anpaws
		}
				
         System.out.println(reverse);  // print statement
	}

}
