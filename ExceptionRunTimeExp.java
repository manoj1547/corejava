public class ExceptionRunTimeExp {  // created a class identifier

	public static void main(String[] args) {  // main method where execution starts
		int a = 12; //variables with initialization
		int b = 15; //variables with initialization
		int c = 0;  //variables with initialization
		
		System.out.println(b/c + c/a);  //Statement contains some arithmetic logic to perform and print on console

	}

}
