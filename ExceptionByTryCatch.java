public class ExceptionByTryCatch {  // created a class identifier

	public static void main(String[] args) {  // main method where execution starts
		int x = 25;    // variable with initialization
		int y = -25;   // variable with initialization
		int z = 0;     // variable with initialization
		
		try   //using try block to handle expected exception in the given logic
		{
			System.out.println((y-x)/z);   //logic to print on the console
		}
		catch(Exception e)  //catch block to handle the remaining set of program statements to print without termination caused by the exception 
		{
			System.out.println("statement inside catch block");  // statement to print on console
			System.out.println(e);    // this statement will print the type of exception caused
		}
		
		System.out.println("statement after all blocks");  //statement to print on the console

	}

}