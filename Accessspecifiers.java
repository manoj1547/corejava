class Employee // creating a class called Employee
  {
	  // private access specifiers can't be accessed outside the class if we want to access it means we should use getters and setters method
	  private int employeId;   //instance variable declared as private
	  private int employeno;   //instance variable declared as private
	  String employename;      //instance variable declared as default  // can be accessed only within the package
	  protected double employesalary;   //instance variable declared as protected --> can be accessed outside the class and package as well through inheritance
  
  //generated getters and setters function in the class where private variables are declared

public int getEmployeId() {   //getter function to get the employeid
	return employeId;
}



public int getEmployeno() {  //getter function to get the employeno
	return employeno;
}



public String getEmployename() {  //getter function to get the employename
	return employename;
}



public double getEmployesalary() {  //getter function to get the employesalary
	return employesalary;
}



public void setEmployeId(int employeId) {  //setter function to set the value for employeid
	this.employeId = employeId;
}



public void setEmployeno(int employeno) {  //setter function to set the value for employeno
	this.employeno = employeno;
}



public void setEmployename(String employename) {  //setter function to set the value for employename
	this.employename = employename;
}



public void setEmployesalary(double employesalary) {   //setter function to set the value for employesalary
	this.employesalary = employesalary;
}
}



public class Accessspecifiers {    //creating another class 
	
	public static void main(String[] args) {   // main function where jvm will understand and execute the things
		Employee e = new Employee();     // creating an object for class employe
		e.setEmployeId(123);            // by using object reference assiagning the value
		e.setEmployename("swapna");     // by using object reference assiagning the value
		e.setEmployeno(99887766);       // by using object reference assiagning the value
		e.setEmployesalary(50000.00);   // by using object reference assiagning the value
		
		System.out.println(e.getEmployeId());    //it will print the satement it contains inside
		System.out.println(e.getEmployename());  //it will print the satement it contains inside
		System.out.println(e.getEmployeno());    //it will print the satement it contains inside
		System.out.println(e.getEmployesalary());  //it will print the satement it contains inside
		
		
		
	}
		

	}


