// reversing the given string by using inbuilt functions

public class ReverseString {   // created a class identifier

	public static void main(String[] args) {  // main function where execution happens
		
		String  m = "mourya";     //since string is immutable we dont have inbuilt method to reverse the given string
		                   
		StringBuffer s = new StringBuffer(m);  // to make use of inbuilt function to reverse the string 
		                                       // we are converting string to stringbuffer
		 String r = s.reverse().toString();    //here we are reversing the given string by using string buffer i.e., s.reverse function
		                                       // to string is used to convert stringbuffer to string and get the output in string only
		
		System.out.println(r);     //it will print the statement inside it contains

	}

}