public class ApyramidPattern // creating a class the first letter should be capital
{
public static void main(String [] args) // creating the main fuction
{
int n = 15; // craeting a variable n and initializing it as 15

for(int i=1; i<=n; i++) // it is the outer for loop to iterate the n(15) number of times
{
     
for (int j =1; j<=n-i; j++) //n=15, i=1 n-i=14 // this will handle the spaces
{
    System.out.print(" ");
}

for (int s=1; s<= (2*i)-1; s++) // created variable s and initialized it as 1 and wrote the condion
   {                             // this will helps us to print the star(*) in nth position in each line

    System.out.print("*");
}
System.out.println("\n"); // it will go to the next line
}
}
}
